export function closest (el, s) {
  let matches = (el.document || el.ownerDocument).querySelectorAll(s),
    i;
  do {
    i = matches.length;
    while (--i >= 0 && matches.item(i) !== el) {};
  } while ((i < 0) && (el = el.parentElement));
  return el;
}
