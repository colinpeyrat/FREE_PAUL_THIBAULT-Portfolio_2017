export function getInertia(destinationValue, value, inertia) {
  var valueToAdd =
    Math.abs((destinationValue - value) * inertia) >= 0.01
      ? (destinationValue - value) * inertia
      : destinationValue - value;
  value += valueToAdd;

  return value;
}
