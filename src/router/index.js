import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/views/HomeView';
import About from '@/views/AboutView';
import Contact from '@/views/ContactView';
import ProjectList from '@/views/ProjectListView';
import Project from '@/views/ProjectView';
import NotFound from '@/views/NotFound';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    },
    {
      path: '/project',
      name: 'ProjectList',
      component: ProjectList
    },
    {
      path: '/project/:slug',
      name: 'Project',
      component: Project
    },
    {
      path: '*',
      name: 'NotFound',
      component: NotFound
    }
  ]
});
