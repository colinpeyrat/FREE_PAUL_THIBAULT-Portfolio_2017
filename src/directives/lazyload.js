const lazyloadImage = (el) => {
  el.dataset.lazyload = "loading";

  const observer = new IntersectionObserver((entries, observer) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        const image = entry.target;

        const onLoad = () => {
          image.dataset.lazyload = "loaded";
          image.removeEventListener("load", onLoad);
        };

        image.addEventListener("load", onLoad);

        image.src = image.dataset.src;

        if (image.dataset.srcset) {
          image.srcset = image.dataset.srcset;
        }

        observer.unobserve(image);
      }
    });
  });

  observer.observe(el);
};

const lazyloadVideo = (el, callback) => {
  const observer = new IntersectionObserver((entries, observer) => {
    entries.forEach(function (entry) {
      const video = entry.target;

      if (entry.isIntersecting) {
        for (const source in video.children) {
          const videoSource = video.children[source];
          if (
            typeof videoSource.tagName === "string" &&
            videoSource.tagName === "SOURCE"
          ) {
            videoSource.src = videoSource.dataset.src;
          }
        }
        const onLoad = () => {
          if (callback) {
            callback();
          }

          video.removeEventListener("canplaythrough", onLoad);
        };

        video.addEventListener("canplaythrough", onLoad);
        video.load();

        observer.unobserve(video);
      }
    });
  });

  observer.observe(el);
};

function bind(el, { value = {} }, vnode) {
  const { callback } = value;

  vnode.context.$nextTick(() => {
    if (el.tagName === "VIDEO") {
      lazyloadVideo(el, callback);
    } else {
      lazyloadImage(el);
    }
  });
}

export default {
  bind,
};
